﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace hw19
{
    public partial class Form1 : Form
    {
        const string USERS_PATH = @"Users.txt";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //when the Enter button is pressed
        private void EnterButton_Click(object sender, EventArgs e)
        {
            string usernameInput = this.textBox1.Text;
            string passwordInput = this.textBox2.Text;
            bool loginFlag = false;

            //reading from the Users file 
            if(!System.IO.File.Exists(USERS_PATH))
            {
                MessageBox.Show("file: \"Users.txt\" does not exist in directory");
            }
            else
            {
                string[] lines = System.IO.File.ReadAllLines(USERS_PATH);

                //checking each line for the user and password and comparing them to the input
                foreach (string item in lines)
                {
                    string[] pair = item.Split(',');

                    //if the username matches
                    if (pair[0].Equals(usernameInput))
                    {
                        //if the password matches
                        if (pair[1].Equals(passwordInput))
                        {
                            loginFlag = true;
                        }
                        break;
                    }
                }

                //if the login was successful
                if (loginFlag)
                {
                    //going to the other form
                    this.Hide();
                    Form calender = new Form2(usernameInput);
                    calender.ShowDialog();

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Incorrect Username or Password");
                }
            }
        }

        //if the cancel button is pressed
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
