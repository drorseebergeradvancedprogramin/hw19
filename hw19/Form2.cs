﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Text.RegularExpressions;

namespace hw19
{
    public partial class Form2 : Form
    {
        string User;
        string filePath;

        //list of birth days - pair <key - date, value - name>
        Dictionary<string, string> BDList = new Dictionary<string, string>();

        //c'tor of Form2
        public Form2(string userName)
        {
            InitializeComponent();
            User = userName;
            filePath = User + "BD.txt";
            dateTimePicker1.Format = DateTimePickerFormat.Short;
        }

        /*
            changes the label to the current day's birth day person if there is one 
        */
        private void MonthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            //getting the date from the event handler
            string date = e.Start.ToString().Split(' ')[0];
            
            //set the default label text  
            string labelText = "On the chosen date, no one has a birth day";

            foreach (string item in BDList.Keys)
            {
                if (date.Equals(item))
                {
                    labelText = "On the chosen date, " + BDList[item] + " has a birth day!";
                    break;
                }
            }
            //changing the label's text
            this.label1.Text = labelText;
        }

        //when the Form is loaded, reading from UserBD.txt and filling the birth day dictionary 
        private void Form2_Load(object sender, EventArgs e)
        {
           

            //finding the userBD.txt file and putting its contents in a list
            if (!System.IO.File.Exists(filePath))
            {
                MessageBox.Show("BD file not found");
            }
            else
            {
                //filling the list of birth days
                string[] lines = System.IO.File.ReadAllLines(filePath);

                foreach (string item in lines)
                {
                    string[] pair = item.Split(',');
                    BDList.Add(pair[1], pair[0]);
                    Console.WriteLine("key = {0}    val = {1}",pair[1], BDList[pair[1]]);
                }
            }
        }

        //adding a person to the birth day file and list
        private void AddButton_click(object sender, EventArgs e)
        {
            //adding a new person to the bd file
            string inputName = this.textBoxName.Text;
            string inputDate = this.dateTimePicker1.Text;
            bool dateFlag = true;
            bool nameFlag = true;

            //checking the date for duplicates 
            foreach (string item in BDList.Keys)
            {
                if(inputDate.Equals(item))
                {
                    dateFlag = false;
                    MessageBox.Show("date already occupied");
                    break;
                }
            }
            //checking for english name
            if (!Regex.IsMatch(inputName, "^[a-zA-Z]*$")) 
            {
                dateFlag = false;
                MessageBox.Show("Invalid name");
            }

            if (dateFlag && nameFlag)
            {
                //adding the person to the file
               
                System.IO.StreamWriter file = System.IO.File.AppendText(filePath);
                file.WriteLine("{0},{1}",inputName,inputDate);

                //adding it to the BDlist
                file.Close();
                BDList.Add(inputDate, inputName);
            }


            //cleaning the textbox and datetimepicker
            this.textBoxName.Text = "";
            this.dateTimePicker1.Text = "";
        }
    }
}
